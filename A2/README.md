## ANLY 502 Assignment 2: Learning MapReduce

***DRAFT: NOT YET RELEASED***

In this assignment you will learn the basics of MapReduce. You will do this with four exercises: 

1. We will rework the filtering exercise of [Assignment
1](../A1/Readme.md) on Hadoop MapReduce using Hadoop's "Streaming"
API. We will do this with a _mapper_ that filters out the lines that
are not wanted, and a _reducer_ that simply copies from input to output.

2. We will then perform the classic "word count" exercise, which creates a histogram of all the words in a text document. The mapper will map single lines to individual words, and the reducer will count the number of words.

3. We will perform a logfile analysis, using web logs from from the website
[https://forensicswiki.org/](https://forensicswiki.org/) between TKDATE and TKDATE. We will generate a report of the number of web "hits" for each month in the period under analysis. 

4. We will then introduce the concept of the _combiner_, which is a
reducer that runs on each mapper before the keys are combined
globally. We will use the combiner to implement an efficient "top-10" pattern that computes the top-10 on each node, minimizing the amount of data that is transfered. 

## Part 1: Basic Filtering with Map Reduce

## Part 2: Word Count with Hadoop Streaming

## Part 3: Logfile analysis

You are to write a several map/reduce programs using Python and Hadoop Streaming that will report the following:

1. How many lines are in the file? Store the results in a file named `q1.txt`.

2. Tally the number of hits in each month of the logfile. For example,
if there were 10,000 hits in the month January 2010 and 20,000 hits in
the month February 2010, your output should look like this:

<pre>
    2010-01 10000
    2010-02 20000
</pre>

    Your output should be sorted.

3. Find the top-10 pages that are 



## Part 4: Finding the top-10 hits

- Extract the URL and generate a histogram of the total number of hits for each URL

- Run a second map/reduce step with a single key, forcing all of the tallies to go through a single process. Implement top-10.

- Run the top-10 on the combiner, performing the data reduction in the nodes, rather than at the every end. 



